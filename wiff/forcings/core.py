import os
from functools import wraps
from logging import basicConfig, DEBUG, getLogger

def enable_logging(level=None, format='%(asctime)s :: %(name)s | %(message)s'):
    level = level or os.getenv('WIFF_LOG_LEVEL') or DEBUG
    basicConfig(level=level, format=format)


class Logging:

    logger = getLogger(__package__)

    def __init__(self, *, logger_root=None, logger_name=None, **kwargs):
        logger_root = logger_root or type(self).logger

        self.logger = logger_root.getChild(type(self).__qualname__)
        if logger_name:
            self.logger = self.logger.getChild(logger_name)

        super().__init__(**kwargs)


class Caching:

    def __init__(self, *, use_cache=True, **kwargs):
        self.__use_cache = use_cache

        super().__init__(**kwargs)

    @classmethod
    def cached_method(cls, func, stateless=True):

        @wraps(func)
        def wrapper(self, *args, _no_cache=False, **kwargs):
            call_func = lambda: func(self, *args, **kwargs)

            if stateless and (_no_cache or not self.__use_cache):
                return call_func()

            attr_name = f'_cached_{func.__qualname__}'

            try:
                return getattr(self, attr_name)
            except AttributeError:
                pass

            return self.__dict__.setdefault(attr_name, call_func() )

        return wrapper

    @classmethod
    def stateful_method(cls, func):
        return cls.cached_method(func, stateless=False)


class WorkPlace:
    pass


class Manager:
    pass
