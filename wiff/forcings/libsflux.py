#===============================================================================
# imports
#===============================================================================
import numpy as np
import netCDF4 as netcdf4
from pathlib import Path

def _specific_humidity(rh, t, p, tC=None):
    '''rh: 0-1, t: kelvin, p: pascal'''
    ## Rdry/Rvap = epsilon
    epsilon = 287.58 / 461.5
    tC = tC or (t-273.16)
    
    es_water = 611.21 * np.exp(17.502 * tC / (t-32.19) )
    es_ice = 611.21 * np.exp(22.587 * tC / (t+20.7) )
    es_t = np.where(es_water - es_ice < .0, es_water, es_ice)
    es = rh * es_t
    
    return epsilon * rh * es_t / (p - (1-epsilon) * es)

def write_sfluxair(date_time, t, latt, long, u, v, p=None, tmp=None, sh=None,
                            rh=None, file='sflux_air_1.001.nc', **ncvar_kwargs):

    """
    write_sfluxair creates sflux_air_1.xxx.nc

    Inputs:
    file - output file (default sflux_air_1.001.nc)
    date_time - initial date
    Variables:
      t = time in days  (time)
      latt = latitude   (ny,nx)
      long = Longitude  (ny,nx)
         u = uwind (time,ny,nx)
         v = uwind (time,ny,nx)
         p = prmsl (time,ny,nx)
         t = stmp  (time,ny,nx)
         sh= shum  (time,ny,nx)"""


    f = netcdf4.Dataset(str(file),'w')
    f.Conventions = 'CF-1.4'
    f.createDimension('time',len(t))
    f.createDimension('nx_grid',long.shape[1])
    f.createDimension('ny_grid',latt.shape[0])

    #Time
    time = f.createVariable('time',np.float64,('time',))
    time[:] = t
    time.long_name = 'Time'
    time.standard_name = 'time'
    time.units = f'days since {date_time:%Y-%m-%d %H:%M:%S}'
    time.base_date = (date_time.year, date_time.month, date_time.day)
    time.calendar = 'julian'

    ##Longitude
    lon = f.createVariable('lon','f',('ny_grid','nx_grid'), **ncvar_kwargs)
    lon[:,:] = long
    lon.long_name = 'Longitude'
    lon.standard_name = 'longitude'
    lon.units = 'degrees_east'

    ##Latitude
    lat = f.createVariable('lat','f',('ny_grid','nx_grid'), **ncvar_kwargs)
    lat[:,:] = latt
    lat.long_name = 'Latitude'
    lat.standard_name = 'latitude'
    lat.units = 'degrees_north'

    ## Uwind (least_significant_digit = 2)
    uwind = f.createVariable('uwind','f',('time','ny_grid','nx_grid'), **ncvar_kwargs)
    uwind[:,:,:] = u
    uwind.long_name = 'Surface Eastward Air Velocity (10m AGL)'
    uwind.standard_name = 'eastward_wind'
    uwind.units = 'm/s'
    uwind.coordinates = 'lon lat'

    ## Vwind (least_significant_digit = 2)
    vwind = f.createVariable('vwind','f',('time','ny_grid','nx_grid'), **ncvar_kwargs)
    vwind[:,:,:] = v
    vwind.long_name = 'Surface Northward Air Velocity (10m AGL)'
    vwind.standard_name = 'northward_wind'
    vwind.units = 'm/s'
    vwind.coordinates = 'lon lat'

    ## prmsl (least_significant_digit = 1)
    if p is not None:
        prmsl = f.createVariable('prmsl','f',('time','ny_grid','nx_grid'), **ncvar_kwargs)
        prmsl[:,:,:] = p
        prmsl.long_name = 'Pressure Reduced to MSL'
        prmsl.standard_name = 'air_pressure_at_sea_level'
        prmsl.units = 'Pa'
        prmsl.coordinates = 'lon lat'

    ## stmp (least_significant_digit = 2)
    if tmp is not None:
        stmp = f.createVariable('stmp','f',('time','ny_grid','nx_grid'), **ncvar_kwargs)
        stmp[:,:,:] = tmp
        stmp.long_name = 'Surface Air Temperature (2m AGL)'
        stmp.standard_name = 'air_temperature'
        stmp.units = 'K'
        stmp.coordinates = 'lon lat'

    ## spfh (least_significant_digit = 5)
    humidity = None
    if sh is not None:
        humidity = sh
    elif rh is not None and tmp is not None and p is not None:
        humidity = _specific_humidity(rh, tmp, p)

    if humidity is not None:
        spfh = f.createVariable('spfh','f',('time','ny_grid','nx_grid'), **ncvar_kwargs)
        spfh[:,:,:] = humidity
        spfh.long_name = 'Surface Specific Humidity (2m AGL)'
        spfh.standard_name = 'specific_humidity'
        spfh.units = '1'
        spfh.coordinates = 'lon lat'

    f.close()

##########################################################################
def write_sfluxrad(date_time, t, latt, long, dl, ds, file='sflux_rad_1.001.nc',
                                                                **ncvar_kwargs):
    """
    write_sfluxrad creates sflux_rad_1.xxx.nc

    Inputs:
    file - output file (default sflux_rad_1.001.nc)
    date_time - initial date
    Variables:
      t = time in days  (time)
      latt = latitude   (ny,nx)
      long = Longitude  (ny,nx)
      dlwrf= Downward Long Wave Radiation Flux  (time,ny,nx)
      dswrf= Downward Short Wave Radiation Flux (time,ny,nx)
      """


    f = netcdf4.Dataset(str(file),'w')
    f.Conventions = 'CF-1.0'
    f.createDimension('time',len(t))
    f.createDimension('nx_grid',long.shape[1])
    f.createDimension('ny_grid',latt.shape[0])

    #Time
    time = f.createVariable('time',np.float64,('time',))
    time[:] = t
    time.long_name = 'Time'
    time.standard_name = 'time'
    time.units = f'days since {date_time:%Y-%m-%d %H:%M:%S}'
    time.base_date = (date_time.year, date_time.month, date_time.day)
    time.calendar = 'julian'

    ##Longitude
    lon = f.createVariable('lon','f',('ny_grid','nx_grid'), **ncvar_kwargs)
    lon[:,:] = long
    lon.long_name = 'Longitude'
    lon.standard_name = 'longitude'
    lon.units = 'degrees_east'

    ##Latitude
    lat = f.createVariable('lat','f',('ny_grid','nx_grid'), **ncvar_kwargs)
    lat[:,:] = latt
    lat.long_name = 'Latitude'
    lat.standard_name = 'latitude'
    lat.units = 'degrees_north'

    ## dlwrf (least_significant_digit = 1)
    dlwrf = f.createVariable('dlwrf','f',('time','ny_grid','nx_grid'), **ncvar_kwargs)
    dlwrf[:,:,:] = dl
    dlwrf.long_name = 'Downward Long Wave Radiation Flux'
    dlwrf.standard_name = 'surface_downwelling_longwave_flux_in_air'
    dlwrf.units = 'W/m^2'
    dlwrf.coordinates = 'lon lat'

    ## dswrf (least_significant_digit = 1)
    dswrf = f.createVariable('dswrf','f',('time','ny_grid','nx_grid'), **ncvar_kwargs)
    dswrf[:,:,:] = ds
    dswrf.long_name = 'Downward Short Wave Radiation Flux'
    dswrf.standard_name = 'surface_downwelling_shortwave_flux_in_air'
    dswrf.units = 'W/m^2'
    dswrf.coordinates = 'lon lat'

    f.close()

##########################################################################
def write_sfluxprc(date_time, t, latt, long, pr, file='sflux_prc_1.001.nc',
                                                                **ncvar_kwargs):
    """
    write_sfluxprc creates sflux_prc_1.xxx.nc

    Inputs:
    file - output file (default sflux_prc_1.001.nc)
    date_time - initial date
    Variables:
      t = time in days  (time)
      latt = latitude   (ny,nx)
      long = Longitude  (ny,nx)
      prate= Surface Precipitation Rate  (time,ny,nx)
      """


    f = netcdf4.Dataset(str(file),'w')
    f.Conventions = 'CF-1.0'
    f.createDimension('time',len(t))
    f.createDimension('nx_grid',long.shape[1])
    f.createDimension('ny_grid',latt.shape[0])

    #Time
    time = f.createVariable('time',np.float64,('time',))
    time[:] = t
    time.long_name = 'Time'
    time.standard_name = 'time'
    time.units = f'days since {date_time:%Y-%m-%d %H:%M:%S}'
    time.base_date = (date_time.year, date_time.month, date_time.day)
    time.calendar = 'julian'

    ##Longitude
    lon = f.createVariable('lon','f',('ny_grid','nx_grid'), **ncvar_kwargs)
    lon[:,:] = long
    lon.long_name = 'Longitude'
    lon.standard_name = 'longitude'
    lon.units = 'degrees_east'

    ##Latitude
    lat = f.createVariable('lat','f',('ny_grid','nx_grid'), **ncvar_kwargs)
    lat[:,:] = latt
    lat.long_name = 'Latitude'
    lat.standard_name = 'latitude'
    lat.units = 'degrees_north'

    ## prate
    prate = f.createVariable('prate','f',('time','ny_grid','nx_grid'), **ncvar_kwargs)
    prate[:,:,:] = pr
    prate.long_name = 'Surface Precipitation Rate'
    prate.standard_name = 'precipitation_flux'
    prate.units = 'kg/m^2/s'
    prate.coordinates = 'lon lat'

    f.close()

##########################################################################
def write_sfluxInputsTxt(date_time, dir_path='.'):
    txt_path = Path(dir_path) / 'sflux_inputs.txt'

    txt_path.write_text(
        date_time.strftime(
            '\n'.join( (
                "&sflux_inputs",
                " start_year  = %Y,",
                " start_month = %m,",
                " start_day   = %d,",
                " start_hour  = %H.,",
                " utc_start   = 00.,",
                "/",
                "",
            ) )
        )
    )



