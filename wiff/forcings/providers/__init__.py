from collections.abc import Mapping, Sequence #, namedtuple
from datetime import time, timedelta
from functools import wraps
from inspect import getmembers
from pathlib import Path
from time import sleep

from ..core import Caching, Logging


class Provider(Logging, Caching):

    class Dataset:

        extent_range = Mapping
        spatial_resolution = (Mapping, float, None)
        grid_size = (Mapping, None)
        time_resolution = timedelta

        period = (timedelta, None)
        time_offset = (timedelta, None)
        daily_updates = (Sequence, None)

        _global_horizontal_range = dict(
            lon = (-180., 180.),
            lat = (-90., 90.),
        )

        _every_twelve_hours = ( time(0), time(12) )
        _every_six_hours = ( time(0), time(6), time(12), time(18) )

        def _every_(interval, *, offset=timedelta(0) ):
            pass

        def _handle_grid_size(self):
            if self.grid_size and not self.spatial_resolution:
                (min_x, max_x), (min_y, max_y) = ( self.extent_range[axis]
                                                    for axis in ('lon', 'lat') )

                self.spatial_resolution = dict(
                    horizontal = (max_x-min_x) / self.grid_size['horizontal'],
                    vertical = (max_y-min_y) / self.grid_size['vertical'],
                )

        def _handle_daily_updates_offset(self):
            return self.daily_updates_offset or timedelta(0)

        def __init__(self, **kwargs):
            data_member_names = list()

            for name, kind in getmembers(self):

                if name.startswith('_'):
                    continue

                value = kwargs.get(name)
                kind_options = kind if isinstance(kind, tuple) else (kind, )

                # if (None in kind_options and value is None) \
                #                                      or isinstance(value, kind):
                if value in kind_options or isinstance(value, kind):
                    setattr(self, name, value)
                else:
                    raise TypeError(
                        f"{type(self).__qualname__}.{name} expected "
                        + ' or '.join( kind.__name__ for kind in kind_options )
                        + f", not {value}!"
                    )

                data_member_names.append(name)

            for name in data_member_names:
                try:
                    getattr(self, f'_handle_{name}')()
                except (AttributeError, TypeError):
                    pass

        def __str__(self):
            return str( vars(self) )

    # dataset_cls = namedtuple('ServiceDataset', 'extent_range spatial_resolution'
    #                                     ' period daily_updates time_resolution')

    # @classmethod
    # def dataset_also_has_(cls, *fields, prefix=''):
    #     flat_fields = tuple( chain.from_iterable(
    #                        nested_fields.split() for nested_fields in fields ) )

    # # def dataset_also_has_(cls, fields, name=None):
    # #     try:
    # #         fields = fields.split()
    # #     except AttributeError:
    # #         pass

    #     return namedtuple(f'{prefix}{cls.dataset_cls.__name__}',
    #                                   (*cls.dataset_cls._fields, *flat_fields) )

    # @classmethod
    # def Dataset(cls, *, grid_size=None, **kwargs):
    #     if grid_size:
    #         (min_x, max_x), (min_y, max_y) = ( kwargs['extent_range']
    #                                                 for axis in ('lon', 'lat') )
    #         kwargs.update(
    #             spatial_resolution = dict(
    #                 horizontal = (max_x - min_x) / grid_size['horizontal'],
    #                 vertical = (max_y - min_y) / grid_size['vertical'],
    #             )
    #         )

    #     new_dataset = cls.dataset_cls(**kwargs)
    #     cls.logger.debug("%s: new dataset with %s %s",
    #                               cls.dataset_cls.__name__, kwargs, new_dataset)
    #     return new_dataset

    datasets = dict()

    def __init__(self, dataset_name, *, date_time, period, extent,
                               work_path='.', create_work_path=False, **kwargs):

        self.dataset = self.datasets[dataset_name]
        self.date_time = self._check_date_time(date_time)
        self.period = self._check_period(period)
        self.extent = self._check_extent(extent)
        self.work_path = self._check_work_path(work_path, create_work_path)

        self.__managed_objects = list()

        super().__init__(**kwargs)

    # def __init_update_arg(self, update):

    #     dataset_updates = self.dataset['daily_updates']

    #     if not update:
    #         self.update = dataset_updates[0]

    #     elif update in dataset_updates:
    #         self.update = update

    #     else:
    #         raise ValueError(
    #                   '{} update is not in {}'.format(update, dataset_updates) )

    # def __init_extent_arg(self, extent):

    #     self.extent = self.dataset['extent_range'].copy()

    #     if extent:
    #         for axis, (min_, max_) in self.extent.items():
    #             within_limits = lambda value: min_ <= value <= max_

    #             if not all( map(within_limits, extent[axis]) ):
    #                 raise ValueError('%s range limits are [%f; %f]', axis,
    #                                                                  min_, max_)

    #         self.extent.update(extent)

    # def __init_period_arg(self, period):

    #     dataset_period = self.dataset['period']

    #     if not period:
    #         self.period = dataset_period
    #         return

    #     if not isinstance(period, timedelta):
    #         raise TypeError('period argument must be a timedelta instance'
    #                                                     f", not {type(period)}")

    #     if timedelta(hours=1) <= period <= dataset_period:
    #         self.period = period

    #     else:
    #         raise ValueError(
    #                'period must be >= 1 hour and <= {}'.format(dataset_period) )

    def _check_date_time(self, date_time):
        return date_time

    def _check_period(self, period):
        return period

    def _check_extent(self, extent):
        return extent

    def _check_work_path(self, path_like, create):
        path = Path(path_like)
        if create:
            path.mkdir(exist_ok=True)
        return path

    def _manage(self, object):
        self.__managed_objects.append(object)
        return object

    def close(self):
        for object in reversed(self.__managed_objects):
            object.close()


    # Decorator limited to methods only
    def retry_on_exception(limit, *, interval=None, filter=None):
        exception_filter = filter or Exception

        def decorator(func):

            @wraps(func)
            def method_wrapper(self, *args, **kwargs):

                for attempt in range(1, limit):
                    try:
                        return func(self, *args, **kwargs)

                    except exception_filter as error:
                        self.logger.warning('ERROR: %s', error)

                        if interval and attempt < limit:
                            self.logger.info(
                                '%i/%i: waiting %s before retrying...',
                                 attempt, limit, interval
                            )
                            sleep( interval.total_seconds() )

                else:
                    raise

            return method_wrapper

        return decorator
