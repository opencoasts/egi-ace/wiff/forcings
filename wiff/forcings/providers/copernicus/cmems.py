import netCDF4
from datetime import datetime, date, time, timedelta
from itertools import chain
from pathlib import Path
from pprint import pformat
from sys import argv
from typing import Sequence

from .. import Provider
from ...targets.schism.open_bnd import OpenBoundaryPhysics2D, \
                             OpenBoundaryPhysics3D, OpenBoundaryBiogeochemical3D


class BaseCMEMS(Provider):

    class Dataset(Provider.Dataset):

        motu_url = (str, None)
        service_id = str
        product_id = str
        #depth_levels = int
        time_offset = timedelta
        variables = Sequence

        _global_afp_service_id = 'GLOBAL_ANALYSIS_FORECAST_PHY_001_024-TDS'
        _ibi_afp_service_id = 'IBI_ANALYSISFORECAST_PHY_005_001-TDS'

        _ibi_horizontal_range = dict(
            lon = (-19., 5.),
            lat = (26., 56.),
        )
        _surface_depth = .49402499198913574
        _depth_range = (_surface_depth, 5727.917)

        _global_spatial_resolution = 1 / 12
        _ibi_spatial_resolution = 1 / 36

        def _handle_extent_range(self):
            depth = self.extent_range.get('depth')
            if isinstance(depth, (float, int, str) ):
                self.extent_range['depth'] = (depth, depth)

    _default_elev_offset_nc = 'GLO-MFC_001_024_mdt.nc'

    def __init__(self, *args, motuclient_path, motuclient_config_path=None,
                                                                      **kwargs):
        super().__init__(*args, **kwargs)

        self.motuclient_path = Path(motuclient_path)
        self.motuclient_config_path = Path(motuclient_config_path)

    _motuclient_base_args = ('--verbose', )

    def _download_product(self, *variables, **motu_kwargs):
        self.logger.info(
            'motuclient: %s | variables: %s | motu_args: %s\n%s',
            self.motuclient_path, ', '.join(variables),
            self._motuclient_base_args, pformat(motu_kwargs)
        )

        flat_args = lambda args: tuple( chain.from_iterable(args) )

        date_fmt = '%Y-%m-%d %H:%M:%S'
        is_date = lambda arg: isinstance(arg, date) or \
                                                       isinstance(arg, datetime)
        arg = lambda kwarg: '--{}'.format( kwarg.replace('_', '-') )
        str_ = lambda value: value.strftime(date_fmt) if is_date(value) \
                                                                 else str(value)
        motu_args = flat_args( ( arg(kwarg), str_(value) )
                                       for kwarg, value in motu_kwargs.items() )

        variables_args = flat_args( ('--variable', var) for var in variables )

        # print(self.motuclient_path, *self._motuclient_base_args, *motu_args,
        #                                                         *variables_args)
        motu_exec = self._run_bin(self.motuclient_path,
                       *self._motuclient_base_args, *motu_args, *variables_args)

        return motu_exec

    _motu_server_url = ''
    _default_dataset_file_path = Path('/tmp/cmems.nc')

    @Provider.retry_on_exception(5, interval=timedelta(seconds=60),
                                                   filter=(FileNotFoundError,) )
    def get_dataset(self, date_time=None, variables=None, file_path=None,
                                                              delete_file=None):
        self.logger.info('get_dataset: date_time=%s', date_time)

        file_path_ = file_path or self._default_dataset_file_path
        (lat_min, lat_max), (lon_min, lon_max) = map( self.extent.__getitem__,
                                                                ('lat', 'lon') )

        ds = self.dataset
        time_offset = ds.time_offset
        spatial_offset = ds.spatial_resolution
        date_time = date_time or self.date_time

        motuclient_kwargs = dict(
            service_id = ds.service_id,
            product_id = ds.product_id,
            date_min = date_time - time_offset,
            date_max = date_time + self.period + time_offset,
            latitude_min = lat_min - spatial_offset,
            latitude_max = lat_max + spatial_offset,
            longitude_min = lon_min - spatial_offset,
            longitude_max = lon_max + spatial_offset,
            out_dir = file_path_.parent,
            out_name = file_path_.name,
        )

        ds_depth_range = ds.extent_range.get('depth')
        if ds_depth_range:
            depth_min, depth_max = self.extent.get('depth') or ds_depth_range
            motuclient_kwargs.update(
                depth_min = depth_min,
                depth_max = depth_max,
            )

        motu_url = ds.motu_url or self._motu_server_url
        if motu_url:
            motuclient_kwargs['motu'] = motu_url

        if self.motuclient_config_path:
            motuclient_kwargs['config-file'] = self.motuclient_config_path

        try:
            self._download_product(
                *(variables or ds.variables),
                **motuclient_kwargs,
            )
            nc_dataset = netCDF4.Dataset(file_path_)

        finally:
            if not file_path or delete_file:
                self.logger.info('unlink: %s', file_path_)
                file_path_.unlink()

        return nc_dataset

    def _open_bnd_path(self):
        return self.work_path

    def _open_bnd_data_time_offset(self):
        return self.dataset.time_offset

    def _open_bnd_data_has_depth(self):
        return 'depth' in self.dataset.extent_range

    def _open_bnd_netcdf_dataset(self, path):
        return self.get_dataset(self.date_time, self.dataset.variables, path)


class CMEMSPhysics3D(BaseCMEMS, OpenBoundaryPhysics3D):


    class Dataset(BaseCMEMS.Dataset):

        _base_variables = ('so', 'thetao')


    datasets = dict(
        global_af = Dataset(
            service_id = Dataset._global_afp_service_id,
            product_id = 'global-analysis-forecast-phy-001-024',

            extent_range = dict(
                Dataset._global_horizontal_range,
                depth = Dataset._depth_range,
            ),
            spatial_resolution = Dataset._global_spatial_resolution,
            #depth_levels = 50,

            period = timedelta(hours=72),
            daily_updates = ( time(0), ),
            time_resolution = timedelta(days=1),
            time_offset = timedelta(hours=12),

            variables = Dataset._base_variables,
        ),
        ibi_af = Dataset(
            service_id = Dataset._ibi_afp_service_id,
            product_id = 'cmems_mod_ibi_phy_anfc_0.027deg-3D_P1D-m',

            extent_range = dict(
                Dataset._ibi_horizontal_range,
                depth = Dataset._depth_range,
            ),
            spatial_resolution = Dataset._ibi_spatial_resolution,
            #depth_levels = 50,

            period = timedelta(hours=72),
            daily_updates = ( time(0), ),
            time_resolution = timedelta(days=1),
            time_offset = timedelta(hours=12),

            variables = Dataset._base_variables,
        ),
    )


class CMEMSPhysics2D(BaseCMEMS, OpenBoundaryPhysics2D):


    class Dataset(BaseCMEMS.Dataset):

        _base_variables = ('zos',)


    datasets = dict(
        global_af = Dataset(
            service_id = Dataset._global_afp_service_id,
            product_id =
                        'global-analysis-forecast-phy-001-024-hourly-t-u-v-ssh',

            extent_range = dict(
                Dataset._global_horizontal_range,
                depth = Dataset._surface_depth,
            ),
            spatial_resolution = Dataset._global_spatial_resolution,
            #depth_levels = 1,

            period = timedelta(hours=72),
            daily_updates = ( time(0), ),
            time_resolution = timedelta(hours=1),
            time_offset = timedelta(minutes=30),

            variables = Dataset._base_variables,
        ),
        ibi_af = Dataset(
            service_id = Dataset._ibi_afp_service_id,
            product_id = 'cmems_mod_ibi_phy_anfc_0.027deg-2D_PT1H-m',

            extent_range = Dataset._ibi_horizontal_range,
            spatial_resolution = Dataset._ibi_spatial_resolution,
            #depth_levels = 0,

            period = timedelta(hours=72),
            daily_updates = ( time(0), ),
            time_resolution = timedelta(hours=1),
            time_offset = timedelta(minutes=30),

            variables = Dataset._base_variables,
        ),
    )


class CMEMSBiogeochemical3D(BaseCMEMS, OpenBoundaryBiogeochemical3D):

    datasets = dict(
        ibi_af = BaseCMEMS.Dataset(
            service_id = 'IBI_ANALYSISFORECAST_BGC_005_004-TDS',
            product_id = 'cmems_mod_ibi_bgc_anfc_0.027deg-3D_P1D-m',

            extent_range = dict(
                BaseCMEMS.Dataset._ibi_horizontal_range,
                depth = BaseCMEMS.Dataset._depth_range,
            ),
            spatial_resolution = BaseCMEMS.Dataset._ibi_spatial_resolution,
            #depth_levels = 0,

            period = timedelta(hours=72),
            daily_updates = ( time(0), ),
            time_resolution = timedelta(days=1),
            time_offset = timedelta(hours=12),

            variables = ('chl', 'nh4', 'no3', 'po4', 'si', 'phyc', 'o2')
        ),
    )


from functools import partial

def download(domain, target, kind, year, month, day, hour, period_hours, \
                                       x_min, x_max, y_min, y_max, levels, dir):
    from ...core import enable_logging
    enable_logging()

    dataset_kind_map = dict(
        global_phys = dict(
            elev = partial(CMEMSPhysics2D, 'global_af'),
            salt = partial(CMEMSPhysics3D, 'global_af'),
            temp = partial(CMEMSPhysics3D, 'global_af'),
        ),
        ibi_phys = dict(
            elev = partial(CMEMSPhysics2D, 'ibi_af'),
            salt = partial(CMEMSPhysics3D, 'ibi_af'),
            temp = partial(CMEMSPhysics3D, 'ibi_af'),
        ),
        ibi_bio = dict(
            eco = partial(CMEMSBiogeochemical3D, 'ibi_af'),
        )
    )

    dir_path = Path(dir)

    cmems_base = partial(dataset_kind_map[domain][kind],
        date_time = datetime( *map(int, (year, month, day, hour) ) ),
        period = timedelta( hours=int(period_hours) ),
        extent = dict(
            lon = ( float(x_min), float(x_max) ),
            lat = ( float(y_min), float(y_max) ),
        ),
        work_path = Path(dir),

        boundaries = (
            dict(
                kind = 'ocean',
                node_count = 47,
            ),
            dict(
                kind = 'river',
                #flux_value = -5.,
                node_count = 2,
            ),
        ),
        timestep = timedelta(seconds=48),

        utils_dir = dir_path / 'utilities',
        templates_dir = dir_path / 'templates',
        elev_offset_nc = dir_path / 'templates/GLO-MFC_001_024_mdt.nc',

        motuclient_path = dir_path / 'motuclient',
        motuclient_config_path = dir_path / 'motuclient.ini',
    )

    cmems = cmems_base() if kind == 'elev' else cmems_base(depth_levels=levels)

    if target == 'open_bnd':

        if kind == 'elev':
            cmems.create_elev2D_th()
        elif kind == 'salt':
            cmems.create_SAL_3D_th()
        elif kind == 'temp':
            cmems.create_TEM_3D_th()
        elif kind == 'bio':
            cmems.create_ECO_3D_th()
        else:
            print(f"No '{kind}' kind!")

    cmems.close()

if __name__ == '__main__':
    from sys import argv
    download(*argv[1:])
