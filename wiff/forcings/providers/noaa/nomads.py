import netCDF4
import numpy
from collections.abc import Mapping
from datetime import datetime, timedelta
from math import floor, ceil

from .. import Provider
from ...targets.sflux import SfluxAir, SfluxRad, SfluxWind


class NOMADS(Provider, SfluxAir, SfluxRad, SfluxWind):

    class Dataset(Provider.Dataset):

        time_offset = (timedelta, None)
        horizontal_360E_axis = bool

        path_template = str
        # variable_map = dict

    # common_variable_map = dict(
    #     wind10_u = 'ugrd10m',
    #     wind10_v = 'vgrd10m',
    #     presure0 = 'msletmsl',
    #     temp2 = 'tmp2m',
    #     sh2 = 'spfh2m',
    #     dlwrf0 = 'dlwrfsfc',
    #     dswrf0 = 'dswrfsfc',
    # )

    datasets = dict(
        gfs_0p25_1hr = Dataset(
            extent_range = Dataset._global_horizontal_range, # convert to source range
            spatial_resolution = .25,
            horizontal_360E_axis = True,

            period = timedelta(hours=120),
            time_resolution = timedelta(hours=1),
            daily_updates = Dataset._every_six_hours,

            # variable_map = common_variable_map

            path_template =
                'gfs_0p25_1hr/gfs{date:%Y%m%d}/gfs_0p25_1hr_{time:%H}z'
        ),
        nam = Dataset(
            extent_range = dict(
                lon = (-152.878623, -49.47263081081),
                lat = (12.219908, 61.20556254545),
            ),
            grid_size = dict(
                horizontal = 913, # 0.113
                vertical = 443, # 0.111
            ),
            horizontal_360E_axis = False,

            period = timedelta(hours=84),
            time_resolution = timedelta(hours=3),
            daily_updates = Dataset._every_six_hours,

            # variable_map = common_variable_map

            path_template = 'nam/nam{date:%Y%m%d}/nam_{time:%H}z',
        ),
        nam_conus = Dataset(
            extent_range = dict(
                lon = (-134.096127, -60.93723597484),
                lat = (21.140671, 52.61339827273),
            ),
            grid_size = dict(
                horizontal = 2502, # 0.029
                vertical = 1155, # 0.027
            ),
            horizontal_360E_axis = False,

            period = timedelta(hours=60),
            time_resolution = timedelta(hours=3),
            daily_updates = Dataset._every_six_hours,

            # variable_map = common_variable_map

            path_template = 'nam/nam{date:%Y%m%d}/nam_conusnest_{time:%H}z',
        ),
        nam_na = Dataset(
            extent_range = dict(
                lon = (148.640594, 357.32093157164),
                lat = (0.897559, 85.301359),
            ),
            spatial_resolution = 0.295,
            horizontal_360E_axis = True,

            period = timedelta(hours=84),
            time_resolution = timedelta(hours=3),
            daily_updates = Dataset._every_six_hours,

            # variable_map = common_variable_map

            path_template = 'nam/nam{date:%Y%m%d}/nam_na_{time:%H}z',
        ),
    )

    _base_url = 'https://nomads.ncep.noaa.gov/dods'

    def _get_dataset_url(self, date_time):
        return f'{self._base_url}/{self.dataset.path_template}'.format(
            date = date_time.date(),
            time = date_time.time(),
        )

    _retry_interval = timedelta(seconds=10)
    _retry_filter = (OSError, RuntimeError)

    @Provider.retry_on_exception(15, interval=_retry_interval,
                                                           filter=_retry_filter)
    def _get_netcdf(self):
        try:
            return self.__netcdf
        except AttributeError:
            pass

        url = self._get_dataset_url(self.date_time)
        self.logger.debug('url=%s', url)

        self.__netcdf = netCDF4.Dataset(url)
        self.logger.debug('netCDF dataset: %s', self.__netcdf)

        return self.__netcdf

    @Provider.retry_on_exception(15, interval=_retry_interval,
                                                           filter=_retry_filter)
    def _prepare(self, force=False, extra_time=timedelta() ):
        try:
            if self.__prepared and not force:
                return
        except AttributeError:
            pass

        (lon_min, lon_max), (lat_min, lat_max) = ( self.extent[axis]
                                                    for axis in ('lon', 'lat') )

        lon_var, lat_var, time_var = ( self._get_netcdf().variables[name][:]
                                            for name in ('lon', 'lat', 'time') )
        #self.logger.debug('lon_var=%s\nlat_var=%s\ntime_var=%s', lon_var, lat_var,
        #                                                               time_var)

        spatial_resolution = self.dataset.spatial_resolution

        if isinstance(spatial_resolution, Mapping):
            horizontal_resolution = spatial_resolution['horizontal']
            vertical_resolution = spatial_resolution['vertical']
        else:
            horizontal_resolution = vertical_resolution = spatial_resolution

        clamp = lambda mode, value, unit: mode(value / unit) * unit

        lon_min_cf = clamp(floor, lon_min, horizontal_resolution)
        lon_max_cc = clamp(ceil, lon_max, horizontal_resolution)
        lat_min_cf = clamp(floor, lat_min, vertical_resolution)
        lat_max_cc = clamp(ceil, lat_max, vertical_resolution)
        self.logger.debug('[clamped] lon: min=%s max=%s | lat: min=%s max=%s',
                                 lon_min_cf, lon_max_cc, lat_min_cf, lat_max_cc)

        #self._lat_bool = lat_min_cf <= lat_var <= lat_max_cc
        self._lat_bool = numpy.logical_and(lat_min_cf <= lat_var,
                                                          lat_var <= lat_max_cc)

        #self.logger.debug('lat_bool=%s', self._lat_bool)
        lat_set = lat_var[self._lat_bool]

        if self.dataset.horizontal_360E_axis:
            from_180WE_to_360E = lambda coord: coord % 360.

            lon_min_cf = from_180WE_to_360E(lon_min_cf)
            lon_max_cc = from_180WE_to_360E(lon_max_cc)
            self.logger.debug('lon_360E= %s : %s', lon_min_cf, lon_max_cc)

        self._inverted_lon_limits = lon_min_cf >= lon_max_cc

        if self._inverted_lon_limits:
            self.logger.debug('inverted_lon_limits!')

            # -180| data ]max ... | 0 | ... min[ data |180
            #    0| data ]max ... |180| ... min[ data |360
            self._lon_bool_max = lon_var <= lon_max_cc
            self._lon_bool_min = lon_min_cf <= lon_var
            self._lon_bool = numpy.logical_or(self._lon_bool_max,
                                                             self._lon_bool_min)

            lon_roll = self._lon_bool_min.sum()
            self.logger.debug('lon_roll=%d', lon_roll)
            lon_set = numpy.roll(lon_var[self._lon_bool], lon_roll)

        else:
            # -180| ... min[ data | 0 | data ]max ... |180
            #    0| ... min[ data |180| data ]max ... |360
            self._lon_bool = numpy.logical_and(lon_min_cf <= lon_var,
                                                          lon_var <= lon_max_cc)

            lon_set = lon_var[self._lon_bool]

        #self.logger.debug('lon_bool=%s', self._lon_bool)

        if self.dataset.horizontal_360E_axis:
            from_360E_to_180WE = \
                                lambda coord: ( ( coord + 180. ) % 360. ) - 180.

            lon_set = from_360E_to_180WE(lon_set)

        self.logger.debug('lon_set[%s]=%s\nlat_set[%s]=%s',
                                 lon_set.shape, lon_set, lat_set.shape, lat_set)

        self._lon_meshgrid, self._lat_meshgrid = numpy.meshgrid(lon_set,
                                                                        lat_set)

        time_offset = self.dataset.time_offset or timedelta()
        period_offset = self.period + extra_time + time_offset
        slice_size = int( ceil(period_offset/self.dataset.time_resolution) )
        self._time_slice = slice(0, slice_size+1)

        time_set = time_var[self._time_slice]
        self._time_relative = time_set - time_set[0]
        self.logger.debug('time_relative=%s', self._time_relative)

        self.__prepared = True
    
    @Provider.retry_on_exception(15, interval=_retry_interval,
                                                           filter=_retry_filter)
    def _variable_data(self, name, empty_head=False):
        self.logger.debug('downloading %s ...', name)

        var = self._get_netcdf().variables[name]
        data = lambda lon_bool: var[self._time_slice, self._lat_bool, lon_bool]

        # it seems that the server doesn't handle noncontiguous regions very
        # well, it's super slow; the noncontiguous regions are due to the domain
        # crossing the prime meridian (Greenwich) (illustrations bellow)
        if self._inverted_lon_limits:
            # 0| data ]max ... |0| ... min[ data |360
            var_data_max = data(self._lon_bool_max)
            var_data_min = data(self._lon_bool_min)
            var_data = numpy.dstack( (var_data_min, var_data_max) )
            self.logger.debug('var_data shapes %s %s %s', var_data_min.shape,
                                             var_data_max.shape, var_data.shape)
        else:
            # 0| ... min[ data |0| data ]max ... |360
            var_data = data(self._lon_bool)

        if empty_head:
            var_data[0] = var_data[1]

        self.logger.debug('downloaded!')
        return var_data

    def _sflux_date_time(self):
        return self.date_time

    def _sflux_work_path(self):
        return self.work_path

    def _sflux_dimensional_data(self):
        self._prepare()
        return dict(
            t = self._time_relative,
            long = self._lon_meshgrid,
            latt = self._lat_meshgrid,
        )

    def _sflux_map_data(self, *, empty_head=False, **variable_map):
        return { sflux_name: self._variable_data(netcdf_name, empty_head)
                           for sflux_name, netcdf_name in variable_map.items() }

    @Provider.cached_method
    def _sflux_wind_data(self):
        return self._sflux_map_data(u='ugrd10m', v='vgrd10m')

    @Provider.cached_method
    def _sflux_air_data(self):
        return dict(self._sflux_wind_data(),
            **self._sflux_map_data(p='msletmsl', sh='spfh2m', tmp='tmp2m')
        )

    @Provider.cached_method
    def _sflux_rad_data(self):
        return self._sflux_map_data(empty_head=True,
                                                   dl='dlwrfsfc', ds='dswrfsfc')


def download(dataset, target, kind, year, month, day, hour, period, \
                                               x_min, x_max, y_min, y_max, dir):
    from ...core import enable_logging
    enable_logging()

    mandeo = NOMADS(dataset,
        date_time = datetime( *map(int, (year, month, day, hour) ) ),
        period = timedelta( hours=int(period) ),
        extent = dict(
            lon = ( float(x_min), float(x_max) ),
            lat = ( float(y_min), float(y_max) ),
        ),
        work_path = dir,
    )

    if target == 'sflux':
        kind_list = kind.split(',')

        if 'air' in kind_list:
            mandeo.create_sflux_air()
        if 'rad' in kind_list:
            mandeo.create_sflux_rad()
        if 'input' in kind_list:
            mandeo.create_inputs_txt()
        if 'wind' in kind_list:
            mandeo.create_sflux_wind()

    mandeo.close()

if __name__ == '__main__':
    from sys import argv
    download(*argv[1:])
