import netCDF4
from datetime import datetime, timedelta
from math import ceil

from .. import Provider
from ...sources.adapters.lftp import LFTP
from ...targets.sflux import SfluxAir, SfluxRad


class Base(Provider):

    class Dataset(Provider.Dataset):

        path_template = str

    datasets = dict(
        sinergea_fc_regional = Dataset(
            extent_range = dict(
                lon = (-9.571, -6.629),
                lat = (36.255, 37.936),
            ),
            grid_size = dict(
                horizontal = 87,
                vertical = 63,
            ),
            time_resolution = timedelta(minutes=15),

            period = timedelta(hours=96),
            time_offset = timedelta(hours=3),
            daily_updates = Dataset._every_six_hours,

            path_template = 'meteo/forecast/3km/wrfout_d02_{:%Y-%m-%d_%H}.nc',
        ),
        sinergea_fc_albufeira = Dataset(
            extent_range = dict(
                lon = (-8.621, -7.817),
                lat = (36.847, 37.406),
            ),
            grid_size = dict(
                horizontal = 72,
                vertical = 63,
            ),
            time_resolution = timedelta(minutes=15),

            period = timedelta(hours=96),
            time_offset = timedelta(hours=3),
            daily_updates = Dataset._every_six_hours,

            path_template = 'meteo/forecast/1km/wrfout_d03_{:%Y-%m-%d_%H}.nc',
        ),
    )

    _default_ftp_site = 'caboruivo.tecnico.ulisboa.pt:21104'
    _default_lftp_settings = (
        'ssl:verify-certificate false',
        'net:max-retries 3',
        'net:timeout 30s',
    )

    def __init__(self, *args, user, password, site=None, lftp_settings=None,
                                                                      **kwargs):
        super().__init__(*args, **kwargs)

        site = site or self._default_ftp_site
        lftp_settings = lftp_settings or self._default_lftp_settings
        self.lftp = LFTP(site, user, password, settings=lftp_settings)

    def _get_netcdf(self):
        try:
            return self.__netcdf
        except AttributeError:
            pass

        range_days = 1 + (self.dataset.period // timedelta(days=1))
        earliest_dataset_datetime = \
            self.date_time + self.dataset.time_offset - self.dataset.period
        print('earliest_dataset_datetime', earliest_dataset_datetime)

        for offset in range(range_days):
            for update_time in reversed(sorted(self.dataset.daily_updates)):

                fetch_date = self.date_time.date() - timedelta(days=offset)
                fetch_datetime = datetime.combine(fetch_date, update_time)
                print('fetch_datetime', fetch_datetime)

                dataset_datetime = fetch_datetime + self.dataset.time_offset
                print('dataset_datetime', dataset_datetime)

                within_range = ( earliest_dataset_datetime <=
                                            dataset_datetime <= self.date_time )

                if not within_range:
                    print('not within_range')
                    continue

                remote_file = self.dataset.path_template.format(fetch_datetime)
                local_file = self.lftp.get_file(remote_file, self.work_path)

                try:
                    self.__netcdf = self._manage(netCDF4.Dataset(local_file))
                except FileNotFoundError:
                    print('file not found')
                    continue
                else:
                    return self.__netcdf


class Sflux(Base, SfluxAir, SfluxRad):

    def _sflux_work_path(self):
        return self.work_path

    def _sflux_date_time(self):
        return self.date_time

    @Provider.cached_method
    def _sflux_time_data(self):
        return 

    def _sflux_map_data(self, **variable_map):
        return { sflux_name: self._get_netcdf().variables[netcdf_name][:]
                           for sflux_name, netcdf_name in variable_map.items() }

    @Provider.cached_method
    def _sflux_dimensional_data(self):
        return dict(
            self._sflux_map_data(latt='lat', long='lon'),
            t = self._sflux_convert_time_units(
                                           self._get_netcdf().variables['time'])
        )

    @Provider.cached_method
    def _sflux_wind_data(self):
        return self._sflux_map_data(u='u10_earth', v='v10_earth')

    @Provider.cached_method
    def _sflux_air_data(self):
        netcdf = self._get_netcdf()
        return dict(
            self._sflux_wind_data(),
            p = self._convert_milibar_to_pascal(netcdf.variables['slp'][:]),
            rh = netcdf.variables['rh2'][:],
            tmp = self._convert_celsius_to_kelvin(netcdf.variables['temp2'][:])
        )

    @Provider.cached_method
    def _sflux_rad_data(self):
        return self._sflux_map_data(dl='lwdown', ds='swdown')


def download(dataset, target, kind, year, month, day, hour, period, dir):
    from ...core import enable_logging
    enable_logging()

    martec = Sflux(dataset,
        date_time = datetime( *map(int, (year, month, day, hour) ) ),
        period = timedelta( hours=int(period) ),
        extent = None,
        work_path = dir,
        user='sinergea', password='T6p7xBPxgwG2u88V'
    )

    if target == 'sflux':
        kind_list = kind.split(',')
        sflux_kwargs = dict(
            dir_path = dir,
        )

        if 'air' in kind_list:
            martec.create_sflux_air(**sflux_kwargs)
        if 'rad' in kind_list:
            martec.create_sflux_rad(**sflux_kwargs)
        if 'input' in kind_list:
            martec.create_inputs_txt(**sflux_kwargs)
        if 'wind' in kind_list:
            martec.create_sflux_wind(**sflux_kwargs)

    martec.close()


if __name__ == '__main__':
    from sys import argv
    download(*argv[1:])
