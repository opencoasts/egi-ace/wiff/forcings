from abc import ABC, abstractmethod

from ..core import Logging


class Source(Logging, ABC):

    # def get_dim_data(self):
    #     pass

    # def get_data(self):
    #     pass

    @abstractmethod
    def close(self):
        pass
