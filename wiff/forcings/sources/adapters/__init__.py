from subprocess import run, PIPE, STDOUT

from ...core import Logging


class SourceAdapter(Logging):

    def _run_exec(self, path, *args, **run_kwargs):

        _run_kwargs = dict(check=True, stdout=PIPE, stderr=STDOUT,
                                              encoding='utf8', errors='replace')
        _run_kwargs.update(run_kwargs)

        args_str = tuple( map(str, args) )
        self.logger.info('%r args=%s run_kwargs=%s', path, args_str,
                                                                    _run_kwargs)

        run_args = (path, *args_str)
        run_proc = run(run_args, **run_kwargs)
        self.logger.info('%r stdout:\n%s', path, run_proc.stdout)

        return run_proc
