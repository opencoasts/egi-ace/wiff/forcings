from pathlib import Path

from . import SourceAdapter


class LFTP(SourceAdapter):

    default_bin_path = '/usr/bin/lftp'

    def __init__(self, site, user=None, password=None, *, settings=(),
                                                       bin_path=None, **kwargs):
        super().__init__(**kwargs)

        self._site = site
        self._user = user
        self._password = password
        self._settings = settings
        self._bin_path = bin_path or self.default_bin_path

    def _run_lftp(self, *cmds, **run_kwargs):
        open_cmd_parts = ['open']
        if self._user:
            open_cmd_parts.append(f'--user {self._user}')
            if self._password:
                open_cmd_parts.append(f'--password {self._password}')
        open_cmd_parts.append( str(self._site) )

        all_cmds = (
            *( f'set {setting}' for setting in self._settings ),
            ' '.join(open_cmd_parts),
            *map(str, cmds),
        )
        all_cmds_arg = '; '.join(all_cmds)
        self._run_exec(self._bin_path, '-c', all_cmds_arg, **run_kwargs)

    def get_file(self, rpath, ldir='.', lname=None):
        _rpath = Path(rpath)
        _ldir = Path(ldir)
        _lname = lname or _rpath.name

        get_cmd = f'get {_rpath} -o {_lname}'
        self._run_lftp(get_cmd, cwd=_ldir)

        return _ldir / _lname
