from pathlib import Path

from . import SourceAdapter


class Motu(SourceAdapter):

    def __init__(self, client_path, service_url , **kwargs):
        super().__init__(**kwargs)

        self.__client_path = Path(client_path)
        self.__service_url = service_url
