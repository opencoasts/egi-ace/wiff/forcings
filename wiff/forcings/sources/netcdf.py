from netCDF4 import Dataset

from .. import Source


class NetCDF(Source):

    def __init__(self, path, **kwargs):
        super().__init__(**kwargs)
        self.dataset = Dataset(path)

    def close(self):
        self.dataset.close()


# from collections import OrderedDict
# from datetime import time as dt_time, timedelta as dt_timedelta
# from functools import lru_cache
# from logging import getLogger
# from numpy import logical_and

# logger = getLogger(__name__)


# # abstract?
# class Source:
#     pass

# # TODO: close datasets
# class NetCDF:

#     # should a class system be used instead? dataclass?
#     datasets = dict(
#         dataset_1 = dict(
#             extent_range = dict(
#                 lon = (-180., 180.),
#                 lat = (-90., 90.),
#             ),
#             spatial_resolution = .25,

#             period = dt_timedelta(hours=72),
#             time_resolution = dt_timedelta(hours=1),
#             daily_updates = ( dt_time(0), ),

#             variable_map = dict(
#                 msl = 'msl',
#                 u10m = 'u10m',
#                 v10m = 'v10m',
#                 t2m = 't2m',
#                 sh = 'sh',
#             ),
#             dim_variable_map = OrderedDict(
#                 time = 'time',
#                 lat = 'latitude',
#                 lon = 'longitude',
#             )
#         )
#     )

#     lru_cache_size = 8

#     logger = logger.getChild(__qualname__)

#     def __init__(self, *, dataset_key, update=None, extent=None, period=None,
#                                                                       **kwargs):

#         self.dataset = self.datasets[dataset_key]
#         self.logger.debug(self.dataset)

#         self.update = self._init_update_arg(update)
#         self.extent = self._init_extent_arg(extent)
#         self.period = self._init_period_arg(period)

#         self._nc_datasets = list()

#         super().__init__(**kwargs)

#     def _init_update_arg(self, update):

#         dataset_updates = self.dataset['daily_updates']

#         if not update:
#             return dataset_updates[0]

#         if update not in dataset_updates:
#             raise ValueError(
#                       '{} update is not in {}'.format(update, dataset_updates) )

#         return update

#     def _init_extent_arg(self, extent):

#         ds_extent_range = self.dataset['extent_range']

#         if not extent:
#             return ds_extent_range

#         init_extent = dict()
#         for axis, (min_, max_) in ds_extent_range.items():

#             extent_axis = extent.get(axis) or tuple()
#             within_limits = lambda value: min_ <= value <= max_

#             if not all( map(within_limits, extent_axis) ):
#                 raise ValueError(f'{axis} range limits are [{min_}; {max_}]')

#             init_extent[axis] = extent_axis

#         return init_extent

#     def _init_period_arg(self, period):

#         dataset_period = self.dataset['period']

#         if not period:
#             return dataset_period

#         if not isinstance(period, dt_timedelta):
#             raise TypeError('period argument must be a timedelta instance'
#                                                         f", not {type(period)}")

#         if dt_timedelta(hours=0) < period <= dataset_period:
#             return period
#         else:
#             raise ValueError(
#                    'period must be > 0 hours and <= {}'.format(dataset_period) )

#     # split this method into download/get _dataset() as a way of abstracting
#     # differences between file and stream based sources
#     @lru_cache(maxsize=lru_cache_size)
#     def get_dataset(self, *, date, **kwargs):

#         nc_dataset = self._get_dataset(date, **kwargs)
#         self._nc_datasets.append(nc_dataset)

#         return nc_dataset

#     def close(self):

#         for nc_ds in self._nc_datasets:
#             nc_ds.close()

#     def _get_dataset(self, date, **kwargs):

#         raise NotImplementedError('_get_dataset MUST be implemented!')

#     @lru_cache(maxsize=lru_cache_size)
#     def get_dataset_variables(self, *, date):

#         return self.get_dataset(date=date).variables

#     @lru_cache(maxsize=lru_cache_size)
#     def _get_dimensional_indexing(self, date):

#         if self.dataset.get('subsetting'):
#             return dict( ( key, slice() )
#                             for key in self.dataset['dim_variable_map'].keys() )

#         variables = self.get_dataset_variables(date=date)
#         dim_variables = dict( ( key, variables[name].data )
#                      for key, name in self.dataset['dim_variable_map'].items() )

#         between = lambda var, min_, max_, epsilon: logical_and(
#                                    min_ - epsilon <= var, var <= max_ + epsilon)

#         dim_bools = dict()

#         lon_var, lat_var = map( dim_variables.get, ('lon', 'lat') )
#         if lon_var and lat_var:

#             (lon_min, lon_max), (lat_min, lat_max) = ( self.extent[axis]
#                                                     for axis in ('lon', 'lat') )

#             spatial_resolution = self.dataset['spatial_resolution']

#             clamp = lambda mode, value, unit: mode(value / unit) * unit
#             clamp_floor = lambda coord: clamp(floor, coord, spatial_resolution)
#             clamp_ceil = lambda coord: clamp(ceil, coord, spatial_resolution)

#             lon_min_cf, lat_min_cf = map( clamp_floor, (lon_min, lat_min) )
#             self.logger.debug('lon_min=%f lat_min=%s', lon_min_cf, lat_min_cf)
#             lon_max_cc, lat_max_cc = map( clamp_ceil, (lon_max, lat_max) )
#             self.logger.debug('lon_max=%s lat_max=%s', lon_max_cc, lat_max_cc)

#             epsilon = spatial_resolution * .1
#             between_ll = lambda var, min_, max_: between(dim_variables[var],
#                                                             min_, max_, epsilon)

#             lon_bool = between_ll('lon', lon_min_cf, lon_max_cc)
#             lat_bool = between_ll('lat', lat_min_cf, lat_max_cc)
#             self.logger.debug('lon_bool=%s\nlat_bool=%s', lon_bool, lat_bool)

#             dim_bools.update(lon=lon_bool, lat=lat_bool)

#         time_var = dim_variables.get('time')
#         if time_var:
#             # time_offset = self.dataset.get('time_offset') or timedelta()
#             # period_offset = self.period + time_offset
#             # slice_size = period_offset /
#             dim_bools['time'] = slice()

#         depth_var = dim_variables.get('depth')
#         if depth_var:
#             dim_bools['depth'] = slice()

#         return dim_bools

#     @lru_cache(maxsize=lru_cache_size)
#     def _get_dimensional_indexing_tuple(self, date):

#         indexing = self._get_dimensional_indexing(date)
#         variables_keys = self.dataset['dim_variable_map'].keys()

#         return tuple( map(indexing.__getitem__, variables_keys) )

#     @lru_cache(maxsize=lru_cache_size)
#     def get_dimensional_data(self, *, date):

#         variables = self.get_dataset_variables(date=date)
#         indexing = self._get_dimensional_indexing(date)
#         dim_variable_data_item = lambda name, key: (key,
#                                               variables[name][ indexing[key] ] )

#         dim_variable_map_items = self.dataset['dim_variable_map'].items()

#         return dict( starmap(dim_variable_data_item, dim_variable_map_items) )

#     def get_variable_data(self, *, date, key):

#         variables = self.get_dataset_variables(date=date)
#         variable_name = self.dataset['variable_map'][key]
#         dimensional_indexing = self._get_dimensional_indexing_tuple(date)

#         return variables[variable_name][dimensional_indexing]
