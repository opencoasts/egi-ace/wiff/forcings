from abc import ABC, abstractmethod

from ..core import Logging


class Target(Logging, ABC):
    pass
