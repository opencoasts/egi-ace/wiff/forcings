import netCDF4
from abc import abstractmethod
from pathlib import Path

from . import Target
from ..libsflux import write_sfluxInputsTxt, write_sfluxair, write_sfluxrad


class _CommonSflux(Target):

    @abstractmethod
    def _sflux_date_time(self):
        pass

    @abstractmethod
    def _sflux_work_path(self):
        pass

    @abstractmethod
    def _sflux_dimensional_data(self):
        pass

    def create_inputs_txt(self, dir_path=None):
        dir_path = dir_path or self._sflux_work_path()
        write_sfluxInputsTxt(self._sflux_date_time(), dir_path)

        self.logger.info('%s saved!', Path(dir_path)/'sflux_inputs.txt')

    _target_time_unit = 'days'
    _date_time_fmt = '%Y-%m-%d %H:%M:%S'

    def _sflux_convert_time_units(self, data, units=None, calendar='standard'):
        try_data_attr_ = lambda name: getattr(data, name, None)
        units = try_data_attr_('units') or units
        calendar = try_data_attr_('calendar') or calendar

        target_units = self._sflux_date_time().strftime(
                        f'{self._target_time_unit} since {self._date_time_fmt}')

        dates = netCDF4.num2date(data, units, calendar)
        return netCDF4.date2num(dates, target_units, calendar)

    @staticmethod
    def _convert_milibar_to_pascal(data):
        '''pa = mbar * 100'''
        return data * 100

    @staticmethod
    def _convert_celsius_to_kelvin(data):
        '''K = C + 273.15'''
        return data + 273.15

    # @abstractmethod
    # def _sflux_time_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_longitude_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_latitude_data(self):
    #     pass


class SfluxWind(_CommonSflux):

    @abstractmethod
    def _sflux_wind_data(self):
        pass

    def create_sflux_wind(self, dir_path=None, *, name='wind.nc', zlib=True):
        dir_path = dir_path or self._sflux_work_path()
        file_path = Path(dir_path) / name

        write_sfluxair(
            file = file_path,
            date_time = self._sflux_date_time(),
            **self._sflux_dimensional_data(),
            **self._sflux_wind_data(),
            fill_value=-999.,
            zlib = zlib,
        )

        self.logger.info('%s saved!', file_path)


# should it be based on SfluxWind?
class SfluxAir(_CommonSflux):

    @abstractmethod
    def _sflux_air_data(self):
        pass

    def create_sflux_air(self, dir_path=None, *, name='sflux_air_1.001.nc',
                                                                     zlib=True):
        dir_path = dir_path or self._sflux_work_path()
        file_path = Path(dir_path) / name

        write_sfluxair(
            date_time = self._sflux_date_time(),
            **self._sflux_dimensional_data(),
            **self._sflux_air_data(),
            file = file_path,
            zlib = zlib,
        )

        self.logger.info('%s saved!', file_path)

    # @abstractmethod
    # def _sflux_wind_u_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_wind_v_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_pressure_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_specific_humidity_data(self):
    #     pass

    # def _sflux_relative_humidity_data(self):
    #     raise NotImplementedError

    # @abstractmethod
    # def _sflux_temperature_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_radiation_short_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_radiation_long_data(self):
    #     pass

    # def create_sflux_air(self, dir_path=None, *, name='sflux_air_1.001.nc',
    #                                                                  zlib=True):
    #     dir_path = dir_path or self._sflux_work_path()
    #     file_path = Path(dir_path) / name

    #     partial_write_sfluxair = partial(write_sfluxair,
    #         file = file_path,
    #         date_time = self._sflux_date_time(),

    #         t = self._sflux_time_data(),
    #         latt = self._sflux_latitude_data(),
    #         long = self._sflux_longitude_data(),

    #         u = self._sflux_wind_u_data(),
    #         v = self._sflux_wind_v_data(),
    #         p = self._sflux_pressure_data(),
    #         tmp = self._sflux_temperature_data(),

    #         zlib = zlib,
    #         #complevel = 4,
    #     )

    #     try:
    #         partial_write_sfluxair( sh=self._sflux_specific_humidity_data() )
    #     except NotImplementedError:
    #         partial_write_sfluxair( rh=self._sflux_relative_humidity_data() )

    #     self.logger.info('%s saved!', file_path)


class SfluxRad(_CommonSflux):

    @abstractmethod
    def _sflux_rad_data(self):
        pass

    def create_sflux_rad(self, dir_path=None, *, name='sflux_rad_1.001.nc',
                                                                     zlib=True):
        dir_path = dir_path or self._sflux_work_path()
        file_path = Path(dir_path) / name

        write_sfluxrad(
            date_time = self._sflux_date_time(),
            **self._sflux_dimensional_data(),
            **self._sflux_rad_data(),
            file = file_path,
            zlib = zlib,
        )

        self.logger.info('%s saved!', file_path)

    # @abstractmethod
    # def _sflux_radiation_long_data(self):
    #     pass

    # @abstractmethod
    # def _sflux_radiation_short_data(self):
    #     pass

    # def create_sflux_rad(self, name='sflux_rad_1.001.nc', dir_path=None, *,
    #                                                                  zlib=True):
    #     dir_path = dir_path or self._sflux_work_path()
    #     file_path = Path(dir_path) / name

    #     write_sfluxrad(
    #         file = file_path,
    #         date_time = self._sflux_date_time(),

    #         t = self._sflux_time_data(),
    #         latt = self._sflux_latitude_data(),
    #         long = self._sflux_longitude_data(),

    #         dl = self._sflux_radiation_long_data(),
    #         ds = self._sflux_radiation_short_data(),

    #         zlib = zlib,
    #         #complevel = 4,
    #     )
    #     self.logger.info('%s saved!', file_path)
