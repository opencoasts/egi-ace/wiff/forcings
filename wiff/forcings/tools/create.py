import argparse

from .. import core

def cli():
    parser = argparse.ArgumentParser()
    parser.add_subparsers

def main():
    core.enable_logging()

if __name__ == '__main__':
    main()